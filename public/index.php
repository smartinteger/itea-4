<?php

declare(strict_types=1);

use uks\Application;

ini_set('display_errors', '1');

include '../vendor/autoload.php';


$config = include '../config/main.php';

$app = Application::getInstance();
$app->configure($config);

$app->run();

/*composer dump-autoload -o необходимо периодически запускать 
для обновления классов для компосера*/