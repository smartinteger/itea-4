# Release Notes

## [v1.0.0 (2020-03-15)]

### Added
- Базовая структура
- Интерфейсы BootstrapInterface, ContainerInterface, RunnableInterface
- Абстрактные классы ComponentAbstract, ContainerAbstract
- Класс Application
- Заготовки для классов компонентов