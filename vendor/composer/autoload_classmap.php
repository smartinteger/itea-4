<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'App\\http\\controllers\\PageController' => $baseDir . '/app/http/controllers/PageController.php',
    'uks\\Application' => $baseDir . '/core/Application.php',
    'uks\\Router' => $baseDir . '/core/Router.php',
    'uks\\contracts\\BootstrapInterface' => $baseDir . '/core/contracts/BootstrapInterface.php',
    'uks\\contracts\\ComponentAbstract' => $baseDir . '/core/contracts/ComponentAbstract.php',
    'uks\\contracts\\ContainerAbstract' => $baseDir . '/core/contracts/ContainerAbstract.php',
    'uks\\contracts\\ContainerInterface' => $baseDir . '/core/contracts/ContainerInterface.php',
    'uks\\contracts\\RunnableInterface' => $baseDir . '/core/contracts/RunnableInterface.php',
);
