<?php

namespace uks\contracts;

interface RunnableInterface
{
    /**
     * Возращает сообщение
     * @return string
     */
    public function run();
  
}