<?php

namespace uks\contracts;

    /**
     * ContainerInterface - должен содержать два метода get и has.
     * 
     */
interface ContainerInterface
{	 
     /**    
     * 
     */
    public function get();
	
	 /**    
     * 
     */
    public function has();
	
	
}