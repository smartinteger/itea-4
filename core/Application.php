<?php

namespace uks;
use uks\contracts\ContainerAbstract;
use uks\contracts\RunnableInterface;
use uks\contracts\ContainerInterface;
use uks\contracts\BootstrapInterface;

use App\Http\Controllers\PageController ;

/* класс Application, расширяет абстрактный класс ContainerAbstract и реализует интерфейсы RunnableInterface и ContainerInterface. */
class Application implements ContainerInterface, RunnableInterface, BootstrapInterface
{
    protected static $instance;

    protected $components = [];

    protected $config = [];

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function run()
    {
		
		$app = Router::getInstance();
		$rout = $app->route($_SERVER['REQUEST_URI']);
		
		$controller = strtolower($rout[1]);//забираем контроллер
		$actons = strtolower($rout[2]);//забираем действие
		$id = explode('/',substr($rout[3], 1));	//забираем пареметры
	
		try {
			//не удалось перехватить при неправильном url пришлось добавить дополнительный клас
		    $controller_class  = 'App\Http\Controllers\\'.ucfirst($controller). 'Controller';	
		    $_controller_class = new $controller_class();
		
			$actons_class  = $actons.'Action';
			
			$_controller_class->$actons_class($id[1]);
			
		} catch (Throwable $t) {
			// Executed only in PHP 7, will not match in PHP 5.x
			echo "перенаправили на Index";
		} catch (Exception $e) {
			// Executed only in PHP 5.x, will not be reached in PHP 7
		}

    }

    public function configure($config)
    {
        $this->config = $config;

        $this->bootstrap();
    }

    public function bootstrap()
    {
        if (isset($this->config['components']) && is_array($this->config['components'])) {
            foreach ($this->config['components'] as $key => $component) {
                if (isset($component['class']) && class_exists($component['class'])) {
                    $instance = new $component['class'];
                    $this->components[$key] = $instance;
                }
            }
        }
    }

    public function has()
    {
        // TODO: Implement has() method.
    }

    public function get()
    {
        // TODO: Implement get() method.
    }
}