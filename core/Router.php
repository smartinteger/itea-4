<?php

namespace uks;

class Router
{   
    protected static $instance;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }


    public function route($url)
    {      
        $pattern = "/(\w+)\/?(\w*)?\??(.*)?$/i";

        if (preg_match($pattern, $url, $matches)) {
            return $matches;
        }
        return false;
    }
   
}